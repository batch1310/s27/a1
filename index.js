const http = require("http");
const PORT = 4000;

http.createServer( (request, response) => {
	
	if(request.url === '/profile' && request.method === 'GET') {
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write('Welcome to your profile.');
		response.end();
	}
	else if(request.url === '/courses' && request.method === 'GET') {
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write('Here\'s our courses available.');
		response.end();
	}
	else if(request.url === '/addCourse' && request.method === 'POST') {
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write('Add course to our resources.');
		response.end();
	}
	else if(request.url === '/updateCourse' && request.method === 'PUT') {
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write('Update a course to our resources.');
		response.end();
	}
	else if(request.url === '/archiveCourse' && request.method === 'DELETE') {
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write('Archive courses to our resources.');
		response.end();
	}
	else{
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write('Welcome to booking system.');
		response.end();
	}
}).listen(PORT, () => console.log(`Your server is running as localhost: ${PORT}`));